![jPlanner Repo](http://i.imgur.com/GIRUs2G.png)

# README #

An application for freelancers who want to store and manage their customers and appointments in a simple app. 

This is a project I'm making for my gf in my spare time.

Note: The database contains 3 columns, named ColorCode1...3. This is because I'm making this app for my girlfriend who is a hair stylist and needs something like that.