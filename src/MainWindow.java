import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.UIManager;

import javax.swing.JTabbedPane;
import javax.swing.JPanel;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.proteanit.sql.DbUtils;

import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.ListSelectionModel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JLabel;

public class MainWindow {

	private JFrame mainFrame;
	private JTable tableCustomerInfo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow(); //Creates the Main Window object
					window.mainFrame.setVisible(true); //Makes Main Window visible
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		});
	}
	
	Connection conn = dbConnection.dbConnector();
	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
		RefreshTables();
		}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		DeleteFromDB delDB = new DeleteFromDB(); //To use the Delete(ID) function
		NewCustomer addCustomerWindow = new NewCustomer(); //Creates the Add Customer dialog window
		
		mainFrame = new JFrame();
		mainFrame.setIconImage(Toolkit.getDefaultToolkit().getImage(MainWindow.class.getResource("/icons/jPlannerLogo.png")));
		mainFrame.setFont(new Font("Verdana", Font.PLAIN, 16));
		mainFrame.setResizable(false);
		mainFrame.setTitle("jPlanner");
		mainFrame.setBounds(100, 100, 1072, 758);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.getContentPane().setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setFont(new Font("Verdana", Font.PLAIN, 14));
		tabbedPane.setBounds(0, 0, 1066, 729);
		mainFrame.getContentPane().add(tabbedPane);
		
		JPanel tabCustomers = new JPanel();
		tabbedPane.addTab("Customers ", new ImageIcon(MainWindow.class.getResource("/icons/customers.png")), tabCustomers, null);
		tabCustomers.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 11, 1041, 579);
		tabCustomers.add(scrollPane);
		
		tableCustomerInfo = new JTable();
		tableCustomerInfo.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableCustomerInfo.setFillsViewportHeight(true);
		tableCustomerInfo.setFont(new Font("Verdana", Font.PLAIN, 17));
		tableCustomerInfo.setRowHeight(35);
		tableCustomerInfo.getTableHeader().setFont(new Font("Dialog", Font.BOLD,16));
		scrollPane.setViewportView(tableCustomerInfo);
		
		JButton addCustomerButton = new JButton("Add New Customer");
		addCustomerButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/add_list.png")));
		addCustomerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addCustomerWindow.setVisible(true);
				addCustomerWindow.addWindowListener(new WindowAdapter() {
					@Override
					public void windowClosed(WindowEvent e) {
						RefreshTables();	
						}
					});
				}
			});
		addCustomerButton.setFont(new Font("Verdana", Font.PLAIN, 14));
		addCustomerButton.setBounds(10, 601, 207, 71);
		tabCustomers.add(addCustomerButton);
		
		JButton deleteButton = new JButton("Delete Entry");
		deleteButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
				delDB.Delete(tableCustomerInfo.getSelectedRow()+1);
				RefreshTables();
				}
			});
		
		deleteButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/delete.png")));
		deleteButton.setFont(new Font("Verdana", Font.PLAIN, 14));
		deleteButton.setBounds(228, 601, 207, 71);
		tabCustomers.add(deleteButton);
		
		JButton exitButton = new JButton("Exit");
		exitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mainFrame.dispose();
			}
		});
		exitButton.setIcon(new ImageIcon(MainWindow.class.getResource("/icons/exit.png")));
		exitButton.setFont(new Font("Verdana", Font.PLAIN, 14));
		exitButton.setBounds(890, 601, 161, 71);
		tabCustomers.add(exitButton);
		
		JPanel tabAppointmens = new JPanel();
		tabbedPane.addTab("Appointmens", (Icon) null, tabAppointmens, null);
		tabAppointmens.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Coming Soon...");
		lblNewLabel.setFont(new Font("Verdana", Font.PLAIN, 16));
		lblNewLabel.setBounds(10, 11, 234, 28);
		tabAppointmens.add(lblNewLabel);
	}
	
	
	public void RefreshTables(){
		//This method is for refreshing the app's tables using the rs2xml.jar library
		
		try{
			String query = "select * from CustomerInfo;";
			PreparedStatement pst = conn.prepareStatement(query);
			ResultSet rs = pst.executeQuery();
			tableCustomerInfo.setModel(DbUtils.resultSetToTableModel(rs));

		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
}
