import java.sql.*;

public class dbConnection {
	
	public static Connection dbConnector(){
		
	 Connection conn = null;
	
	 try {
	      Class.forName("org.sqlite.JDBC");
	      
	      conn = DriverManager.getConnection("jdbc:sqlite:main.db");
	      return conn;
	      
	    } catch ( Exception e ) {
	    	
	      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	      
//	      System.exit(0);
	      
	      return null;
	    }
	}
	 
}
